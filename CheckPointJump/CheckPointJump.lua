CheckPointJump = LibStub("AceAddon-3.0"):NewAddon("CheckPointJump", "AceConsole-3.0", "AceEvent-3.0" );
local enabled=false;
local listall={};
local namechanel="CPJ";
function CheckPointJump:OnInitialize()
		-- Called when the addon is loaded
		SLASH_CPJ1, SLASH_CPJ2 = '/cpj','/CheckPointJump';
		SlashCmdList["CPJ"]=function(msg, editbox)
			local command, arg1, arg2 = msg:match("^(%S*)%s*(%S*)%s*(%S*)$");
				if command == "add" and arg1~="" and arg2~="" then 
					self:Add(arg1,arg2);
				elseif command == "del" and arg1~="" and arg2~="" then
					self:Remove(arg1,arg2);
				elseif command == "delallcp" and arg1~="" then
					self:RemoveAllTeleportToUser(arg1);
				elseif command == "delallusers" and arg1~="" then
					self:RemoveAllUsersToTeleport(arg1);
				elseif command == "addraidcp" and arg1~="" then
					--print("Add all users in raids to teleport("..arg1..")");
					print("Not working!");
				elseif command == "delall" then
					listall={};
					print("Deleted all.");
				elseif command == "listall" or command == "all" or command == "list" then
					self:ListAll();
				elseif command == "listusers" and arg1~="" then
					self:ListAllUsersToTeleport(arg1);
				elseif command == "listcp" and arg1~="" then
					self:ListAllTeleportsToUser(arg1);
				elseif command == "on" then
					enabled=true;
					print("|cff00ccff Addon enabled.");
				elseif command == "off" then
					enabled=false;
					print("|cff00ccff Addon disabled.");
				elseif command == "help" then
					print("List command:");
					print("on");
					print("off");
					print("add");
					print("del");
					print("listall");
					print("listusers");
					print("listcp");
					print("delallcp");
					print("delallusers");
					print("delall");
					print("help");
					print("addraidcp");
				else
				print("Use /cpj help");
				end
		end
		self:RegisterEvent("CHAT_MSG_CHANNEL");
		-- Print a message to the chat frame
		self:Print("Addon welcomes you.");		
end


function CheckPointJump:CHAT_MSG_CHANNEL(event, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg11, arg12)
	local findedId=self:FindedElement(arg2,arg1);
	if arg9==namechanel and findedId>0 and enabled==true then
		print(arg2,arg1,arg8);
		SendChatMessage(".tele name "..arg2.." "..arg1 , "CHANNEL", nil, arg8); 
	end
end

function CheckPointJump:Add(name,tele)
	if self:FindedElement(name,tele) == -1 then
		table.insert(listall,{name=name,tele=tele});
		print("|cff00ccff Added user("..name..") to teleport("..tele..")");
	else
		print("|cff00ccff Not added user("..name..") to teleport("..tele.."), because this user is in base.");
	end
end

function CheckPointJump:FindedElement(name,tele)
local i=1;
	for item in pairs(listall) do
		if listall[item].name == name and listall[item].tele == tele then
			return i;
		else
			i=i+1;
		end
	end
return -1;
end

function CheckPointJump:ListAll()
	print("|cff00ccff List users and teleports:");
	for item in pairs(listall) do		
		print("|cff00ff00 " .. listall[item].name, "|cffffff00 " .. listall[item].tele);
	end
end

function CheckPointJump:Get(id)
local i=1;
	for item in pairs(listall) do
		if i==id then
			return listall[item];
		else
			i=i+1;
		end
	end
return -1;
end

function CheckPointJump:Remove(name,tele)
local findedid=self:FindedElement(name,tele);
	if findedid > 0 then
		table.remove(listall,findedid);
		print("|cff00ccff Deleted user("..name..") to teleport("..tele..")");
	else
		print("|cff00ccff Not deleted, because this element("..name.." , "..tele..") isn't in base.");
	end	
end

function CheckPointJump:GetAllTeleportToUser(username)
local result={}
	for item in pairs(listall) do
		if listall[item].name == username then
			table.insert(result,listall[item]);
		end
	end
return result;
end

function CheckPointJump:RemoveAllTeleportToUser(username)
local count=#listall;
local i=1;
	while i<=count do
		if listall[i].name == username then
			table.remove(listall,i);
			count=count-1;
		else
			i=i+1;
		end
	end
	print("|cff00ccff Deleted all teleports to user("..username..")");
end

function CheckPointJump:RemoveAllUsersToTeleport(teleport)
local count=#listall;
local i=1;
	while i<=count do
		if listall[i].tele == teleport then
			table.remove(listall,i);
			count=count-1;
		else
			i=i+1;
		end
	end
	print("|cff00ccff Deleted all users to teleport("..teleport..")");
end

function CheckPointJump:ListAllUsersToTeleport(teleport)
print("|cff00ccff List users to teleport("..teleport.."):");
	for item in pairs(listall) do		
		if listall[item].tele == teleport then
			print("|cff00ff00 " .. listall[item].name);
		end
	end
end

function CheckPointJump:ListAllTeleportsToUser(username)
print("|cff00ccff List teleports to user("..username.."):");
	for item in pairs(listall) do		
		if listall[item].name == username then
			print("|cffffff00 " .. listall[item].tele);
		end
	end
end

